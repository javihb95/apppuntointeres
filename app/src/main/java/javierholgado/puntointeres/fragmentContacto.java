package javierholgado.puntointeres;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class fragmentContacto extends Fragment {
    TextView textweb;
    TextView textTelefono;
    TextView textCorreo;
    public fragmentContacto() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View  v =  inflater.inflate(R.layout.fragment_fragment_contacto, container, false);
        textweb = (TextView) v.findViewById(R.id.textWeb);
        textweb.setText("Web del sitio");
        textTelefono = (TextView) v.findViewById(R.id.textTelefono);
        textTelefono.setText(MainActivity.sitioSeleccionado.getTelefono());
        textCorreo = (TextView) v.findViewById(R.id.textCorreo);
        textCorreo.setText(MainActivity.sitioSeleccionado.getCorreo());
        // Inflate the layout for this fragment
        return v;
    }

}
