package javierholgado.puntointeres;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public static Sitio sitioSeleccionado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_main);
        MyPagerAdapter myPagerAdapter =
                new MyPagerAdapter(getSupportFragmentManager());
        ViewPager viewPager = (ViewPager) findViewById(R.id.VistaFragments);
        viewPager.setAdapter(myPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        Intent intent = getIntent();
        int posicion = intent.getIntExtra(listaActivity.POSICION, 0);
        sitioSeleccionado = ListSitios.arraySitio.get(posicion);
    }
    public void clickWeb(View v){
        Uri paginaWeb = Uri.parse(MainActivity.sitioSeleccionado.getWeb());
        Intent webIntent = new Intent(Intent.ACTION_VIEW, paginaWeb);
        startActivity(webIntent);
    }
    public void clickTelefono(View v){
        Uri telefono = Uri.parse(MainActivity.sitioSeleccionado.getTelefono());
        Intent webIntent = new Intent(Intent.ACTION_DIAL, telefono);
        startActivity(webIntent);
    }
    public void clickCorreo(View v){
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {MainActivity.sitioSeleccionado.getCorreo()}); // destinatarios
        startActivity(emailIntent);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
class MyPagerAdapter extends FragmentStatePagerAdapter {

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment;
        switch(i) {
            case 0:
                fragment = new FragmentDetallePrincipal();
                break;
            case 1:
                fragment = new fragmentContacto();
                break;
            case 2:
                fragment = new FragmentFotos();
                break;
            case 3:
                fragment = new fragment_mapas();
                break;
            default:
                fragment = null;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Detalle";
            case 1:
                return "Contacto";
            case 2:
                return "Fotos";
            case 3:
                return "Mapas";
        }
        return null;
    }

}
