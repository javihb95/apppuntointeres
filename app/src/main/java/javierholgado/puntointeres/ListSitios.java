package javierholgado.puntointeres;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2DAW9 on 14/11/2016.
 */

public class ListSitios {
    public static List<Sitio> arraySitio = new ArrayList();

    public List<Sitio> getArrraysitio() {
        return arraySitio;
    }

       public void setArrraysitio(List<Sitio> arraySitio) {
        this.arraySitio = arraySitio;
    }

    public static void anadirSitios(){
        //http://101lugaresincreibles.com/2010/09/10-paisajes-que-parecen-de-fantasia-en-espana.html
        Sitio sitioNuevo;

        sitioNuevo = new Sitio();
        sitioNuevo.setId(1);
        sitioNuevo.setTitulo("Torcal de Antequera");
        sitioNuevo.setDireccion("Entre los municipios de Antequera y Villanueva de la Concepción");
        sitioNuevo.setDescripcion("se lo conoce por las caprichosas formas que los diversos agentes " +
                "erosivos han ido modelando en sus rocas calizas, constituyendo un destacado ejemplo" +
                " de paisaje kárstico. En el año 1929 se reconoce al área como el primer Espacio " +
                "Natural Protegido Andaluz de interés Nacional , en 1978 se le declara Parque de la reserva Natural2 ," +
                " en 1989 Paraje Natural . En 2014 se declara Zona Especial de Conservación ." +
                " La sierra goza de la declaración de Zona Especial para la Protección de las Aves," +
                " emitida por la Consejería de Medio Ambiente y Ordenación del Territorio de la Junta de Andalucía.");
        sitioNuevo.setWeb("Web del Sitio");
        sitioNuevo.setImagen("ubrique");
        arraySitio.add(sitioNuevo);

        sitioNuevo = new Sitio();
        sitioNuevo.setId(2);
        sitioNuevo.setTitulo("Segundo sitio de la lista");
        sitioNuevo.setDireccion("URb. del segundo Sitio nº3");
        sitioNuevo.setWeb("Web del Sitio");
        sitioNuevo.setImagen("ubrique");
        arraySitio.add(sitioNuevo);

        sitioNuevo = new Sitio();
        sitioNuevo.setId(3);
        sitioNuevo.setTitulo("Tercer sitio de la lista");
        sitioNuevo.setDireccion("Calle asd nº3");
        sitioNuevo.setWeb("Web del Sitio");
        sitioNuevo.setImagen("ubrique");
        arraySitio.add(sitioNuevo);
    }

}
