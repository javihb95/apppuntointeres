package javierholgado.puntointeres;

/**
 * Created by 2DAW9 on 14/11/2016.
 */

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
    // each data item is just a string in this case
        public TextView tituloElemento;
        public TextView direccionElemento;
        public ImageView imagenElemento;
        public ViewHolder(View v) {
            super(v);
            tituloElemento = (TextView) v.findViewById(R.id.textTitulo);
            direccionElemento = (TextView) v.findViewById(R.id.textDireccion);
            imagenElemento = (ImageView) v.findViewById(R.id.imageView);
        }

    }
    public MyRecyclerViewAdapter(Context context) {
        this.context = context;

    }
    // Create new views (invoked by the layout manager)
    @Override
    public MyRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_elemento, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        int id = context.getResources().getIdentifier(
                "drawable/" + ListSitios.arraySitio.get(position).getImagen() ,
                "drawable",
                context.getPackageName());
        Log.d("mostrar",String.valueOf(id));
        holder.tituloElemento.setText(ListSitios.arraySitio.get(position).getTitulo());
        holder.direccionElemento.setText(ListSitios.arraySitio.get(position).getDireccion());
        holder.imagenElemento.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), id, null));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return ListSitios.arraySitio.size();
    }
}
