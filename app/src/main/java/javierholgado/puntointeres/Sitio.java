package javierholgado.puntointeres;

/**
 * Created by 2DAW9 on 10/11/2016.
 */

public class Sitio {
    private int id;
    private String titulo;
    private String descripcion;
    private String imagen;
    private String direccion;
    private String telefono;
    private String web;
    private String correo;
    private float latitud;
    private float longitud;

    public Sitio(int id, String titulo, String descripcion,
                 String imagen, String direccion, String telefono, String web, String correo, float latitud,float longitud) {
        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.direccion = direccion;
        this.telefono = telefono;
        this.web = web;
        this.correo = correo;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public Sitio() {
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getTelefono() {

        return telefono;
    }
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public float getLatitud() {
        return latitud;
    }

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }
}
