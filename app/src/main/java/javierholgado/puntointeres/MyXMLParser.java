package javierholgado.puntointeres;

/**
 * Created by 2DAW9 on 28/11/2016.
 */
import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MyXMLParser {
    private static final String ns = null; // We don't use namespaces

    // TODO: Especificar las etiquetas generales que se usan en el documento XML
    private String XML_START_TAG = "Sitios";
    private String ELEMENT_TAG = "sitio";

    public List parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return parseXMLDocument(parser);
        } finally {
            in.close();
        }
    }

    private List parseXMLDocument(XmlPullParser parser) throws XmlPullParserException, IOException {
        List entries = new ArrayList();

        parser.require(XmlPullParser.START_TAG, ns, XML_START_TAG);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tagName = parser.getName();
            // Starts by looking for the entry tag
            if (tagName.equals(ELEMENT_TAG)) {
                entries.add(readEntry(parser));
            } else {
                skip(parser);
            }
        }
        return entries;
    }

    // TODO: Retornar el tipo de objeto que se va descargar
    private Sitio readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        // TODO: Crear una variable para cada atributo de los objetos a descargar
        int id = 0;
        String titulo = null;
        String descripcion = null;
        String imagen = null;
        String direccion = null;
        String telefono = null;
        String web = null;
        String correo = null;
        float latitud = 0;
        float longitud = 0;
        parser.require(XmlPullParser.START_TAG, ns, ELEMENT_TAG);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tagName = parser.getName();
            // TODO: Preguntar por cada posible etiqueta que se encuentra en los elementos XML
            if (tagName.equals("id")) {
                // TODO: Leer el contenido indicando de nuevo la etiqueta, y convirtiendo si es necesario
                id = Integer.valueOf(readContent(parser, "id"));
            } else if (tagName.equals("titulo")) {
                titulo = readContent(parser, "titulo");
            } else if (tagName.equals("descripcion")){
                descripcion = readContent(parser, "descripcion");
            } else if(tagName.equals("imagen")){
                imagen= readContent(parser,"imagen");
            } else if (tagName.equals("direccion")){
                direccion = readContent(parser,"direccion");
            }else if (tagName.equals("telefono")){
                telefono= readContent(parser,"telefono");
            }else if (tagName.equals("web")){
                web= readContent(parser,"web");
            }else if (tagName.equals("correo")){
                correo= readContent(parser,"correo");
            }else if (tagName.equals("latitud")){
                latitud= Float.parseFloat(readContent(parser,"latitud"));
            }else if (tagName.equals("longitud")){
                longitud= Float.parseFloat(readContent(parser,"longitud"));
            }else {
                skip(parser);
            }
        }
        // TODO: Retornar un objeto creado a partir de las variables anteriores
        return new Sitio(id, titulo, descripcion,imagen,direccion,telefono,web,correo,latitud,longitud);
    }

    private String readContent(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
        String result = "";
        parser.require(XmlPullParser.START_TAG, ns, tag);
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        parser.require(XmlPullParser.END_TAG, ns, tag);
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
