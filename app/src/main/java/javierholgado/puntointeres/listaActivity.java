package javierholgado.puntointeres;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class listaActivity extends AppCompatActivity {
        private RecyclerView mRecyclerView;
        private RecyclerView.Adapter mAdapter;
        private RecyclerView.LayoutManager mLayoutManager;
        private ListSitios listaSitios = new ListSitios();
        private static final String URL = "http://pruebasandroid.esy.es/ListaSitios.xml";
        public final static String POSICION = "Posicion punto interes seleccionado";


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mAdapter = new MyRecyclerViewAdapter(getApplicationContext());
            setContentView(R.layout.activity_lista);
            //if (ListSitios.arraySitio.isEmpty()){
                new DownloadXmlTask().execute(URL);
            //}else{
              //  mRecyclerView.setAdapter(mAdapter);
            //}

            mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            mRecyclerView.setHasFixedSize(true);

            // use a linear layout manager
            mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(),
                    mRecyclerView, new ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    //Sitio sitioNuevo = listaSitios.getArrraysitio().get(position);
                    //Toast.makeText(getApplicationContext(), sitioNuevo.getTitulo() , Toast.LENGTH_SHORT).show();
                    Intent intentMainActivity = new Intent(getApplicationContext(), MainActivity.class);
                    intentMainActivity.putExtra(POSICION, position);
                    startActivity(intentMainActivity);
                }



            }));
        }
        public interface ClickListener {
        void onClick(View view, int position);
        }
        private class DownloadXmlTask extends AsyncTask<String, Void, Integer> {
            @Override
            protected Integer doInBackground(String... urls) {
                try {
                    InputStream stream = null;
                    try {
                        stream = downloadUrl(urls[0]);
                        MyXMLParser myXmlParser = new MyXMLParser();
                        ListSitios.arraySitio = myXmlParser.parse(stream);
                    } finally {
                        if (stream != null) {
                            stream.close();
                        }
                    }
                    // Descarga correcta
                    return 0;
                } catch (IOException e) {
                    // Error de conexión
                    return 1;
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                    // Error en los datos
                    return 2;
                }
            }

            @Override
            protected void onPostExecute(Integer resultCode) {
                switch(resultCode) {
                    case 0:
                        // TODO: Realizar aquí el tratamiento oportuno de la lista de objetos que se
                        //  ha descargado
                        mRecyclerView.setAdapter(mAdapter);
                        break;
                    case 1:
                        // TODO: Mostrar los mensajes de error en el lugar oportuno
                        Context context = getApplicationContext();
                        Toast toast = Toast.makeText(context, "Error de conexión", Toast.LENGTH_SHORT);
                        toast.show();
                        Log.w(this.getClass().getName(), "Error de conexión");
                        break;
                    case 2:
                        Context cont = getApplicationContext();
                        toast = Toast.makeText(cont, "Error de conexión", Toast.LENGTH_SHORT);
                        toast.show();
                        Log.w(this.getClass().getName(), "Error en los datos");
                        break;
                }
            }
        }

        // Given a string representation of a URL, sets up a connection and gets
        // an input stream.
        private InputStream downloadUrl(String urlString) throws IOException {
            //(No hace el import automáticamente ¿?) import java.net.URL;
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            return conn.getInputStream();
        }

    }
