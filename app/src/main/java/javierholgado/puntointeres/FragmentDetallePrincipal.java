package javierholgado.puntointeres;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDetallePrincipal extends Fragment {
    TextView tituloSitio;
    TextView descripcion;
    ImageView imagen;
    public FragmentDetallePrincipal() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment_detalle_principal, container, false);
        tituloSitio = (TextView) v.findViewById(R.id.TituloSitio);
        descripcion = (TextView) v.findViewById(R.id.textViewDescripcion);
        descripcion.setText(MainActivity.sitioSeleccionado.getDescripcion());
        tituloSitio.setText(MainActivity.sitioSeleccionado.getTitulo());
        imagen = (ImageView) v.findViewById(R.id.imagenDetalle);
        int id = v.getContext().getResources().getIdentifier(
                "drawable/" + MainActivity.sitioSeleccionado.getImagen() ,
                "drawable",
                v.getContext().getPackageName());
        imagen.setImageDrawable(ResourcesCompat.getDrawable(v.getContext().getResources(), id, null));
        // Inflate the layout for this fragment
        return v;
    }

}
